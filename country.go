package main

// Country : Stores information about each country
type Country struct {
	Code string `json:"code"`
	Name string `json:"name"`
}
