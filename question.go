package main

// Question : Stores country codes for the correct answer and alternate options
type Question struct {
	Answer  Country   `json:"answer"`
	Options Countries `json:"options"`
}
