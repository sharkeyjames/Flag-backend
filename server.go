package main

import (
	"fmt"
	"log"
	"net/http"
)

const port string = ":8080"

func main() {
	http.HandleFunc("/ping/", ping)
	http.HandleFunc("/hello/", hello)
	http.HandleFunc("/countries/", countries)
	http.HandleFunc("/question/", generateQuestion)
	// http.HandleFunc("/question/", question)
	http.HandleFunc("/", noHandler)

	fmt.Println("Listening on localhost" + port)
	log.Fatal(http.ListenAndServe(port, nil))
}
