package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
)

func noHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "No handler for route: %s\n", r.URL.Path)
}

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, World!")
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong")
}

func countries(w http.ResponseWriter, r *http.Request) {
	countries := getCountries()

	// Returning JSON
	w.Header().Add("Content-Type", "application/json")

	// Prevent CORS error
	w.Header().Add("Access-Control-Allow-Origin", "http://localhost:3000")

	json.NewEncoder(w).Encode(countries)
}

func generateQuestion(w http.ResponseWriter, r *http.Request) {
	countries := getCountries()
	answer := countries[rand.Intn(len(countries))]

	var options Countries
	for {
		country := countries[rand.Intn(len(countries))]
		if country.Code != answer.Code {
			options = append(options, country)
		}
		if len(options) == 3 {
			break
		}
	}
	question := Question{Answer: answer, Options: options}

	// Returning JSON
	w.Header().Add("Content-Type", "application/json")

	// Prevent CORS error
	w.Header().Add("Access-Control-Allow-Origin", "http://localhost:3000")

	json.NewEncoder(w).Encode(question)
}

func trimFirstChars(x int, s string) string {
	for i := range s {
		if i > x {
			return s[i:]
		}
	}
	return s[:0]
}

func getCountries() Countries {
	var countries Countries
	raw, err := ioutil.ReadFile("./data/countries.json")
	if err != nil {
		fmt.Println(err.Error()) // Still to do; error handling
	}
	json.Unmarshal(raw, &countries)
	return countries
}
